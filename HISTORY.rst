=======
History
=======

0.3.1 (2019-10-27)
------------------

* Implement CI 
* Add single test for model creation

0.3.0 (2019-10-27)
------------------

* Convert character based model to factory method instead of stateful model
* Update docker env


0.2.0 (2019-03-31)
------------------

* Implement grid search using luigi

0.1.0 (2019-03-17)
------------------

* First release on PyPI.
