from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
import pickle
from sklearn.model_selection import train_test_split
from sklearn.metrics import log_loss
from tensorflow.keras.models import Model
from tensorflow.keras.layers import CuDNNLSTM, LSTM, TimeDistributed,GlobalAveragePooling1D, GlobalMaxPooling1D, Flatten,Conv1D,Embedding, Dense, Bidirectional, MaxPooling1D, BatchNormalization, Dropout, Input
from tensorflow.keras import optimizers
from tensorflow.keras.utils import to_categorical
import numpy as np
import pandas as pd
from keras_metrics import binary_precision, binary_recall, binary_f1_score
from util_kit.modeling.losses import WeightedLoss

from ..utils.resources import prioritize_gpu

    
def build_charlstm(x_data, y_data,     
                    embedding_size  = 100, 
                    lstm_layer0     = 75,
                    lstm_layer1     = 50,
                    lstm_layer2     = 25,
                    dropouts        = .1 ,
                    dense_layer0    = 50,
                    dense_layer1    = 50,
                    dense_layer2    = 50,
                    pooling         = 'max', 
                    final_activation= 'sigmoid',
                    optimizer       = 'adam'     
    ):
    """Build a character level LSTM model for text classification
    
    Args:
        x_data (np.array): the input text data, tokenized
        y_data (np.array): the output labels to predict (training!)
        embedding_size (int, optional): The embedding size. Defaults to 100.
        lstm_layer0 (int, optional): Add a bidirectional lstm.  If None, skipped. Defaults to 75.
        lstm_layer1 (int, optional): Add a bidirectional lstm.  If None, skipped. Defaults to 50.
        lstm_layer2 (int, optional): Add a bidirectional lstm.  If None, skipped. Defaults to 25.
        dropouts (float, optional): Adds a dropbout layer. Defaults to .1.
        dense_layer0 (int, optional): Add a dense layer of size.  If None, skipped. Defaults to 50.
        dense_layer1 (int, optional): Add a dense layer of size.  If None, skipped. Defaults to 50.
        dense_layer2 (int, optional): Add a dense layer of size.  If None, skipped. Defaults to 50.
        pooling (str, optional): 'average', or 'max' for global pooling. Defaults to 'max'.
        final_activation (str, optional): The final activation on the last dense layer. Defaults to 'sigmoid'.
        optimizer (str, optional): The keras optimizer function. Defaults to 'adam'.
    
    Raises:
        ValueError: If pooling layer is not allowed
    
    Returns:
        Model: a built and compiled model for modeling
    """
    input_layer = Input(shape=(x_data.shape[1],), name='input_layer')
    x = Embedding(output_dim=embedding_size, input_dim=len(np.unique(x_data)))(input_layer)
    for l in [lstm_layer0, lstm_layer1, lstm_layer2]:
        if l:
            x = Bidirectional(CuDNNLSTM(l, return_sequences=True))(x)
            x = BatchNormalization()(x)
            x = Dropout(dropouts)(x)
        else:
            x = Dropout(dropouts)(x)

    for l in [dense_layer0, dense_layer1, dense_layer2]:
        if l:
            x = Dense(l)(x)
            x = Dropout(dropouts)(x)

    x = Dense(y_data.shape[1])(x)

    if pooling == 'max':
        x = GlobalMaxPooling1D()(x)
    elif pooling == 'average':
        x = GlobalAveragePooling1D()(x)
    else:
        raise ValueError(f'{pooling} is not a valid pooling setting')

    x = Dropout(dropouts)(x)
    x = Dense(y_data.shape[1], activation=final_activation)(x)
    model = Model(input_layer, x)
    model.compile(optimizer=optimizer, 
                  loss=WeightedLoss(y_data), 
                  metrics=['accuracy', binary_precision(), binary_recall(), binary_f1_score()])
    return model
