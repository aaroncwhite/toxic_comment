from unittest import TestCase
from sklearn.datasets import make_multilabel_classification

from .charlstm import *


class TestModelCreation(TestCase):
    """Only test model creation for now
    """

    def test_creation(self):
        x,y = make_multilabel_classification()

        m = build_charlstm(x, y)