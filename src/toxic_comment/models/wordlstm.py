from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import CuDNNLSTM, LSTM, TimeDistributed,GlobalAveragePooling1D, GlobalMaxPooling1D, \
                            Flatten,Conv1D,Embedding, Dense, Bidirectional, MaxPooling1D, BatchNormalization, Dropout, Input
from tensorflow.keras import optimizers
from tensorflow.keras.regularizers import l1_l2
from tensorflow.keras.utils import to_categorical
from keras_metrics import categorical_f1_score, categorical_precision, categorical_recall
import pandas as pd
import numpy as np
import tensorflow as tf

from util_kit.modeling.model import BaseModel
from util_kit.modeling.losses import WeightedLoss

# Test for a GPU, and if one exists, take advantage of the 
# CUDA power available.
try:
    assert tf.test.is_gpu_available()
    LSTM_LAYER = CuDNNLSTM
except:
    LSTM_LAYER = LSTM


class WordLSTM(BaseModel):
    """Creates a Keras model using the functional API
    """

    # Defaults that can be overridden
    embedding_size     = 300
    lstm_size           = 300
    conv_filters        = 300
    conv_activation     = 'sigmoid'
    conv_padding        = 'valid'
    conv_kernel         = 5
    conv_strides        = 5       

    pool_size           = 2
 
    dropouts            = .05

    dense0              = 50
    dense1              = 50

    
    regularizer_l1      = .001
    regularizer_l2      = .001

    final_activation    = 'sigmoid'

    loss                = 'weighted'
    optimizer           = 'adam'

    batch_size = None
    epochs = None


    def build(self, x_data, y_data):
        """Builds the model using defaults or any 
        overriden hyperparameters and sets input, output,
        and weighted loss functions to match the data
        
        Arguments:
            x_data {dict or numpy.Array} -- dict of arrays or a single array to use
            y_data {dict or numpy.Array} -- dict of arrays or single array to use for y
        """
        # Text layer
        input_layer = Input(shape=(x_data.shape[1],), name='text_word')

        x = Embedding(output_dim=self.embedding_size, input_dim=len(np.unique(x_data)), name='embedding')(input_layer)
        x = Conv1D(filters=self.conv_filters,
                    activation=self.conv_activation,
                    kernel_size=self.conv_kernel,
                    strides=self.conv_strides)(x)
        x = MaxPooling1D(pool_size=self.pool_size)(x)
        x = Dropout(self.dropouts)(x)
        x = Bidirectional(LSTM_LAYER(self.lstm_size))(x)
        x = Dropout(self.dropouts)(x)
        for d in [self.dense0, self.dense1]:
            if d:
                x = Dense(d)(x)
                x = Dropout(self.dropouts)(x)
        
        x = Dense(y_data.shape[1], activation=self.final_activation, 
                              name='text_word_pred', 
                              activity_regularizer=l1_l2(l1=self.regularizer_l1, 
                                                         l2=self.regularizer_l2))(x)
        model = Model(input_layer, x)

        if self.loss == 'weighted':
            loss = WeightedLoss(y_data)
        else: 
            loss = self.loss

        model.compile(loss=loss,
                    optimizer=self.optimizer,
                    metrics=[categorical_f1_score(), categorical_precision(), categorical_recall()])

        self.model = model
