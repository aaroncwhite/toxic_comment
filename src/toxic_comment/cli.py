from luigi import build
from .tasks.cleaning import TokenizeAndSplitData
# from .tasks.grid_search import MakeGrid
# from .tasks.tpe_search import TuneCharModel
from .tasks.model import RunCharModel

def main(args=None):
    build([ RunCharModel(data_directory='data/inputs')], local_scheduler=True, scheduler_host='luigi', scheduler_port=8082)