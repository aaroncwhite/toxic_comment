import subprocess, re
import tensorflow as tf
import numpy as np
from contextlib import contextmanager

@contextmanager
def prioritize_gpu():
    with tf.device('/gpu:{}'.format(pick_gpu_lowest_memory())):
        yield 


# Nvidia-smi GPU memory parsing.
# Tested on nvidia-smi 370.23
# Courtesy of 
# https://stackoverflow.com/questions/41634674/tensorflow-on-shared-gpus-how-to-automatically-select-the-one-that-is-unused

def run_command(cmd):
    """Run command, return output as string."""
    output = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True).communicate()[0]
    return output.decode("ascii")

def list_available_gpus():
    """Returns list of available GPU ids."""
    output = run_command("nvidia-smi -L")
    # lines of the form GPU 0: TITAN X
    gpu_regex = re.compile(r"GPU (?P<gpu_id>\d+):")
    result = []
    for line in output.strip().split("\n"):
        m = gpu_regex.match(line)
        assert m, "Couldnt parse "+line
        result.append(int(m.group("gpu_id")))
    return result

def gpu_memory_map():
    """Returns map of GPU id to memory allocated on that GPU."""

    # lines of the form
    # |    0      8734    C   python                                       11705MiB |
    memory_regex = re.compile(r"(\d{1,}MiB\s+/\s+\d{1,}MiB)")
    result = {gpu_id: 0 for gpu_id in list_available_gpus()}
    for gpu in result.keys():
        output = run_command("nvidia-smi -i {}".format(gpu))
        m = memory_regex.search(output)
        if not m:
            continue
        result[gpu] +=  np.divide(*[int(x.strip()) for x in m.group().replace('MiB', '').split('/')])

        n_proc = len(output[output.find('GPU Memory:')].split('\n'))
        result[gpu] *= n_proc
    return result

def pick_gpu_lowest_memory():
    """Returns GPU with the least allocated memory"""

    memory_gpu_map = [(memory, gpu_id) for (gpu_id, memory) in gpu_memory_map().items()]
    best_memory, best_gpu = sorted(memory_gpu_map)[0]
    return best_gpu