from tensorflow.keras import backend as K
import tensorflow as tf
import functools

def as_keras_metric(method):
    """Wraps a TensorFlow metric to be used with Keras

    Arguments:
        method (tf callable): a tensorflow callable 

    Returns:
        Keras metric: a wrapped metric usable with Keras models
    
    """
    @functools.wraps(method)
    def wrapper(self, args, **kwargs):
        """ Wrapper for turning tensorflow metrics into keras metrics """
        value, update_op = method(self, args, **kwargs)
        K.get_session().run(tf.local_variables_initializer())
        with tf.control_dependencies([update_op]):
            value = tf.identity(value)
        return value
    return wrapper


def precision_metric():
    """Wraps tensorflow precision metric
    
    Returns:
        Keras metric: precision metric
    """

    return as_keras_metric(tf.metrics.precision)


def recall_metric():
    """Wraps tensorflow recall metric
    
    Returns:
        Keras metric: recall metric
    """

    return as_keras_metric(tf.metrics.recall)