from luigi import Task, WrapperTask, Parameter, LocalTarget, DictParameter
import yaml
import json
from itertools import tee
from util_kit.luigi.descriptors import Requires, Requirement,SaltedOutput,JsonImport, YamlImport, FileHash
from util_kit.modeling.hyperparameters import HyperparameterConfig

from .external import HyperparameterGridConfig
from .cleaning import TokenizeAndSplitData
# from .model import CharModel

class MakeGrid(Task):

    grid_loc = Parameter(default='data/grid', description="Where to store the grid parameters. Ideally, this is not salted in case the grid expands.")
    requires = Requires()
    config = Requirement(HyperparameterGridConfig)
    data = Requirement(TokenizeAndSplitData)

    output = SaltedOutput("{task.grid_loc}/hyperparameters-{salt}.yaml",
                          LocalTarget,
                          salt_params=False,
                          salt_source=False,
                          add_salt="{task.config.hash}")

    def requires(self):
        config = self.config.import_file()

        config = HyperparameterConfig(**config)

        grid = config.grid()
        write_params, run_model = tee((WriteModelParams(grid_loc = self.grid_loc, 
                                params= self.params_str(g)) for g in grid))

        yield write_params
        yield (CharModel(config_loc=g.output().path, 
                         data_directory=self.data.output().path) for g in run_model)

    def params_str(self, params):
        # Tried with YAML but it doesn't know how to deal with tuples
        return json.dumps(params, sort_keys=True)

    def run(self):
        with self.output().open('w') as f:
            f.write(self.params_str(self.config.import_file()))



class WriteModelParams(Task):

    hash = FileHash()

    import_file = JsonImport()
    params = Parameter()
    grid_loc = Parameter()

    output = SaltedOutput("{task.grid_loc}/grid-search/{salt}/model-config-{salt}.yaml",
                            LocalTarget,
                            salt_params=False,
                            salt_source=False,
                            add_salt="{task.params}"
                            )

    def run(self):
        with self.output().open('w') as f:
            f.write(self.params)
