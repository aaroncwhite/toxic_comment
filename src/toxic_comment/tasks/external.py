from luigi import ExternalTask, Parameter, LocalTarget, format
import os
import urllib3
from util_kit.luigi.descriptors import FileHash, YamlImport, PandasCSVImport

class InputData(ExternalTask):
    """A pointer to input data, either training or
    test data with a small amount of pre-processing
    during import. 

    Usage:
    data = InputData(loc='data/train.csv').import_file()

    """

    hash = FileHash()
    import_file = PandasCSVImport()

    input_directory = Parameter(description="The base folder.")

    def output(self):
        return LocalTarget(os.path.join(self.input_directory,'train.csv'))


    def run(self):
        # Download the file if it doesn't exist
        # confirmed via file hash that this is the same file as direct from Kaggle
        # sha256:bd4084611bd27c939ba98e5e63bc3e5a2c1a4e99477dcba46c829e4c986c429d
        with urllib3.PoolManager() as http:
            r = http.request('GET', 'https://github.com/ipcplusplus/toxic-comments-classification/raw/master/train.csv')
            with self.output().open('w') as f:
                f.write(r.data.decode())


class DataConfig(ExternalTask):
    """Pointer to the data grouping configuration file
    """

    hash = FileHash()
    import_file = YamlImport()
    
    config_loc = Parameter('configs/data_config.yaml')

    def output(self):
        return LocalTarget(self.config_loc)


class HyperparameterGridConfig(ExternalTask):
    """Pointer to hyperparameter configuration
    """

    hash = FileHash()
    import_file = YamlImport()

    config_loc = Parameter('configs/hyperparameter_config.yaml')

    def output(self):
        return LocalTarget(self.config_loc)


class ModelConfig(ExternalTask):
    """Pointer to model configuration
    """

    hash = FileHash()
    import_file = YamlImport()

    config_loc = Parameter()

    def output(self):
        return LocalTarget(self.config_loc)

                