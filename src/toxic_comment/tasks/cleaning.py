from luigi import Task, ExternalTask, WrapperTask, Parameter, LocalTarget, FloatParameter, ListParameter, format
from sklearn.model_selection import train_test_split
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
import dask.dataframe as dd
import pandas as pd
import pickle
import yaml
import os
import shutil

from util_kit.luigi.descriptors import Requires, Requirement, SaltedOutput
from util_kit.luigi.target import DaskParquetTarget

from .external import InputData, DataConfig

class TokenizeAndSplitData(Task):
    """Tokenizes and splits the data based on the split 
    parameters defined.  The data will be tokenized and split 
    into three parts- train, validation, and test for both x and y

    Data will be split first on `test_split` and then the non-training
    data are split on `val_split`.  

    Parameters:
        data_dir    {str}   -- Where the output data will be stored
        test_split  {float} -- How much to set aside for testing
        val_split   {float} -- How much to set aside for validation


    Requires:
        CleanDataFiles -- The data must be combined into a single file

    """

    input_directory = Parameter(description="Where the input data reside")
    model_directory = Parameter(description="Where the split data should reside")

    requires= Requires()
    data = Requirement(InputData, file_name='train.csv', low_memory=False)
    config = Requirement(DataConfig, config_loc='configs/data_config.yaml')

    output = SaltedOutput("{task.model_directory}-{salt}", LocalTarget, 
                            salt_params=False, 
                            salt_source=False,
                            add_salt="{task.config.hash}")

    def import_files(self):
        return {
            'train': {
                'char' : dd.read_parquet(os.path.join(self.output().path, 'train/char')),
                'words': dd.read_parquet(os.path.join(self.output().path, 'train/words')),
                'y'    : dd.read_parquet(os.path.join(self.output().path, 'train/y'))
            },
            'validation': {
                'char' : dd.read_parquet(os.path.join(self.output().path, 'validation/char')),
                'words': dd.read_parquet(os.path.join(self.output().path, 'validation/words')),
                'y'    : dd.read_parquet(os.path.join(self.output().path, 'validation/y'))
            },
            'test': {
                'char' : dd.read_parquet(os.path.join(self.output().path, 'test/char')),
                'words': dd.read_parquet(os.path.join(self.output().path, 'test/words')),
                'y'    : dd.read_parquet(os.path.join(self.output().path, 'test/y'))
            }
        
        }

    def run(self):
        data   = self.data.import_file()
        config = self.config.import_file()

        char_tokenizer = Tokenizer(**config['tokenizer_settings']['char']['init'])
        word_tokenizer = Tokenizer(**config['tokenizer_settings']['word']['init'])
        
        char_tokenizer.fit_on_texts(data['comment_text'])
        word_tokenizer.fit_on_texts(data['comment_text'])

        data['any_toxic'] = (data.iloc[:,2:].sum(axis=1) > 0).astype(int)

        split_settings  = config['split_settings']

        train_val, test = train_test_split(data, test_size=split_settings['test_split'],
                                                 stratify=data['any_toxic'])
        
        train, val      = train_test_split(train_val, test_size=split_settings['test_split'],
                                                      stratify=train_val['any_toxic'])



        # Now we have train, val, test
        # create output dicts
        train_data_char, train_data_words = self.make_split(train, 
                                        char_tokenizer,
                                        config['tokenizer_settings']['char']['pad'], 
                                        word_tokenizer,
                                        config['tokenizer_settings']['word']['pad'])

        val_data_char, val_data_words     = self.make_split(val, 
                                        char_tokenizer,
                                        config['tokenizer_settings']['char']['pad'], 
                                        word_tokenizer,
                                        config['tokenizer_settings']['word']['pad'])

        test_data_char, test_data_words   = self.make_split(test, 
                                        char_tokenizer,
                                        config['tokenizer_settings']['char']['pad'], 
                                        word_tokenizer,
                                        config['tokenizer_settings']['word']['pad'])                                     

        with self.output().temporary_path() as tp:
            self.write(tp, 'train', train_data_char, train_data_words, train.iloc[:, 2:], char_tokenizer, word_tokenizer)
            self.write(tp, 'validation', val_data_char, val_data_words, val.iloc[:, 2:], char_tokenizer, word_tokenizer)
            self.write(tp, 'test', test_data_char, test_data_words, test.iloc[:, 2:], char_tokenizer, word_tokenizer)

            with open(os.path.join(tp, 'char_tokenizer.pkl'), 'wb') as f:
                pickle.dump(char_tokenizer, f)

            with open(os.path.join(tp, 'word_tokenizer.pkl'), 'wb') as f:
                pickle.dump(word_tokenizer, f)

            shutil.copy(self.config.output().path, os.path.join(tp, 'data_config.yaml'))
           

    def write(self, base_loc, split_type, char, words, y, char_tokenizer, word_tokenizer):
        loc = os.path.join(base_loc, split_type)
        if not os.path.exists(loc):
            os.makedirs(loc)

        dd.from_pandas(char, npartitions=32).to_parquet(os.path.join(loc, 'char'))
        dd.from_pandas(words, npartitions=32).to_parquet(os.path.join(loc,'words'))
        dd.from_pandas(y, npartitions=32).to_parquet(os.path.join(loc,'y'))




    def make_split(self, df, char_tokenizer, char_padding, word_tokenizer, word_padding):
        x_char  = char_tokenizer.texts_to_sequences(df['comment_text'])
        x_words = word_tokenizer.texts_to_sequences(df['comment_text'])

        x_char  = pad_sequences(x_char, **char_padding)
        x_words = pad_sequences(x_words, **word_padding)

        x_char  = pd.DataFrame(x_char, index=df.index)
        x_words = pd.DataFrame(x_words, index=df.index)

        x_char.columns = x_char.columns.astype(str)
        x_words.columns = x_words.columns.astype(str)

        return x_char, x_words

