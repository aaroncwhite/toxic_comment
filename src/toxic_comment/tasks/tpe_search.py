from luigi import Task, Parameter, LocalTarget, IntParameter
import os
import json
import dask.dataframe as dd
import logging
import tensorflow as tf
from tensorflow.keras.callbacks import TensorBoard, EarlyStopping

from hyperopt import Trials, STATUS_OK, tpe, fmin
from util_kit.modeling.hyperparameters import HyperparameterConfig

import util_kit.modeling.hyperparameters as hyperparameters
from util_kit.modeling.hyperparameters import HyperoptParameter

hyperparameters.HyperasParameter = hyperparameters.HyperoptParameter


from util_kit.luigi.descriptors import SaltedOutput, FileHash, Requires, Requirement
from .external import ModelConfig, HyperparameterGridConfig
from .cleaning import TokenizeAndSplitData
# from ..models.charlstm import CharLSTM

import itertools
import yaml
import json
import pprint
from hyperopt import hp

class HyperparameterConfig(object):

    def __repr__(self):
        return pprint.pformat(self.params(), indent=2)
    
    def __init__(self, **params):
        
        for k,v in params.items():
            if v.get('hyperopt'):
                v = HyperoptParameter.from_dict(v['params'])
            
            else:
                v = v['params']
            
            setattr(self, k, v)
    
    def params(self):
        return {k : getattr(self, k) for k in dir(self) 
                if not k.startswith('_') and (not callable(getattr(self, k)) or isinstance(getattr(self, k), HyperoptParameter))}
    
  
    @classmethod
    def from_yaml(cls, filepath):
        """Imports a configuration defined in a YAML file
        
        Arguments:
            filepath {str or os.Path} -- Where the file is located

        Returns:
            HyperparameterConfig -- An instance of the Hyperparameter config
        """
        with open(filepath, 'r') as f:
            config = yaml.safe_load(f)

        return cls(**config)

    def to_yaml(self, filepath):
        """Saves a HyperparameterConfig to disk
        
        Arguments:
            filepath {str or os.Path} -- where the file should be saved
        """

        with open(filepath, 'w') as f:
            msg = ['# Hyperparameter Configuration file', 
                    '# Reload with HyperparameterConfig.from_yaml(this_file_path)\n\n']
            f.write('\n'.join(msg))
            tmp = json.dumps(self.to_dict())
            tmp = json.loads(tmp)
            f.write(yaml.dump(tmp, default_flow_style=False))


    def to_dict(self):
        out = {}
        for k,v in self.params().items():
            if isinstance(v, HyperoptParameter):
                out[k] = {'hyperopt': True, 'params' : v.to_dict()}
            else:
                out[k] = {'params': v}
        return out
            
    def get_space(self):
        x = self.params()
        out = {}
        for k,v in x.items():
            out[k] = v if not isinstance(v, HyperoptParameter) else v()
            
        return out
    
    
class HyperoptParameter:
    
    def __init__(self, distribution, *args, **kwargs):
        self.distribution = distribution
        self.args = args
        if kwargs.get('name'):
            self.name = kwargs.pop('name')
            
        self.kwargs = kwargs
        self._validate()
        
    def _validate(self):
        if self.distribution not in hp.__dict__.keys():
            raise TypeError("{} is not a known parameter estimator from hyperopt.hp".format(self.distribution))

    
    def __get__(self, config, cls):
        if config:
            self.name = self._name(config)
            return self
        else:
            return self.__class__
        
    def __repr__(self):
        return "{}({})".format(self.__class__.__name__, self.__dict__)
    
    def __call__(self):
        return hp.__dict__[self.distribution](self.name, *self.args, **self.kwargs)
    

    @classmethod
    def from_dict(cls, dictionary):
        distribution = dictionary.pop('distribution', None)
        args = dictionary.pop('args', ())
        name = dictionary.pop('name', None)
        kwargs = dictionary.pop('kwargs', {})
        return cls(distribution, name=name, *args, **kwargs)
    
    def to_dict(self):
        return {k:v for k,v in self.__dict__.items() if v}
    
    def _name(descriptor, instance):
        if not instance:
            return ''
        
        attributes = set()
        for cls in type(instance).__mro__:
            # add all attributes from the class into `attributes`
            # you can remove the if statement in the comprehension if you don't want to filter out attributes whose names start with '__'
            attributes |= {attr for attr in dir(cls) if not attr.startswith('__')}
        for attr in attributes:
            try:
                if type(instance).__dict__[attr] is descriptor:
                    
                    return attr
            except:
                pass


log = logging.getLogger('luigi-interface')

class TuneCharModel(Task):

    max_evals = IntParameter(default=40)
    requires = Requires()
    config = Requirement(HyperparameterGridConfig)
    data = Requirement(TokenizeAndSplitData)

    runs = 0
    resources = {'gpu': 2}

    def output(self):
        directory = os.path.split(self.config.output().path)[0]
        return LocalTarget(os.path.join(directory, 'results.json'))

    def tune(self, space):
        try:
            tf.keras.backend.clear_session()
            config = tf.ConfigProto()
            config.gpu_options.allow_growth = True
            with tf.Session(config=config) as s:
                print(str(space))
                model = CharLSTM(**space)
                model.build(self.x_train, self.y_train)
                s.run(tf.global_variables_initializer())

                log.debug(model.summary())
            
                model.fit(self.x_train, self.y_train,
                          epochs=space['epochs'],
                          batch_size=space['batch_size'],
                          validation_split=.3,
                          verbose=2,
                          callbacks=[TensorBoard(log_dir='/logs/hyperopt-tuning/{}/{}'.format(self.config.hash[0:6], self.runs)),
                                     EarlyStopping(patience=5, min_delta=.001)]
                          )

                model.evaluate(self.x_val, self.y_val, batch_size=space['batch_size'])
                score = 1 - model.evaluate_results['f1_score']
        except:
            score = 100

        self.runs += 1
        return {'loss' : score, 'status': STATUS_OK}

    def run(self):
        config = HyperparameterConfig(**self.config.import_file())
        model_data = self.data.import_files()
        self.x_train = model_data['train']['char'].compute().values
        self.y_train = model_data['train']['y'].compute().values
        self.x_val = model_data['validation']['char'].compute().values
        self.y_val = model_data['validation']['y'].compute().values

        results = fmin(
            self.tune,
            algo=tpe.suggest,
            space=config.get_space(),
            trials=Trials(),
            max_evals=self.max_evals
        )
        log.debug(str(results))
        with self.output().open('w') as f:
            f.write(json.dumps(results))
