from luigi import Task, Parameter, LocalTarget, IntParameter
import os
from pathlib import Path
import json
import dask.dataframe as dd
import logging
import tensorflow as tf
import numpy as np
from tensorflow.keras.callbacks import TensorBoard, EarlyStopping

from util_kit.luigi.descriptors import SaltedOutput, FileHash, Requires, Requirement
from .external import ModelConfig
from .cleaning import TokenizeAndSplitData
from ..models.charlstm import build_charlstm


log = logging.getLogger('luigi-interface')

class RunCharModel(Task):
    """Run the character level LSTM model 
    to predict toxic comments.  Model outputs 
    trained weights based on the input parameters
    and hashed config to a "salted" directory.
    """
    config_loc = Parameter()
    data_directory = Parameter()
    epochs = IntParameter(default=1)
    batch_size = IntParameter(default=256)
    stop_patience = IntParameter(default=5)

    requires = Requires()
    config = Requirement(ModelConfig)
    data = Requirement(TokenizeAndSplitData)

    # Output is a salted directory where we will save 
    # the model results, any additional outputs we think 
    # are useful
    output = SaltedOutput('data/results/{task.build.__name__}/{salt}/weights.h5', LocalTarget,
                          salt_source=False,
                          salt_params=True,
                          add_salt='{task.config.hash}')
  
    def build(self, *args, **kwargs):
        return build_charlstm(*args, **kwargs)

    def get_data(self, text_type='char'):
        # These are all currently parquet data sources
        # and need to be converted to pandas.  An 
        # alternative could be to write a Sequencer 
        # that only fetches the data necessary per batch.  
        # Because the data fit in RAM, this is less necessary
        # right now, but the pieces are in place for future
        # development
          
        model_data = self.data.import_files()
        train_x, train_y = self._get_data(model_data['train'], text_type)
        val_x, val_y = self._get_data(model_data['validation'], text_type)
        test_x, test_y = self._get_data(model_data['test'], text_type)
        
        return train_x, train_y, \
                val_x, val_y, \
                test_x, test_y


    def _get_data(self, data_set, text_type='char'):
        x = data_set[text_type].compute()
        y = data_set['y'].compute()
        # Pad the data so model doesn't bomb out 
        # with smaller than batch samples
        
        x = np.vstack([
            x,
            np.zeros((int(np.ceil(
                        x.shape[0] / self.batch_size)* self.batch_size - x.shape[0]), 
                        x.shape[1]))
        ])
        y = np.vstack([
            y,
            np.zeros((int(np.ceil(
                        y.shape[0] / self.batch_size)* self.batch_size - y.shape[0]), 
                        y.shape[1]))        
        ])
        return x, y
   

    def run(self):
        train_x, train_y, val_x, val_y, test_x, test_y = self.get_data()

        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        with tf.Session(config=config) as s:
            model = self.build(train_x, train_y, 
                               **self.config.import_file())

            # Since Keras Metrics is using... Keras we need
            # to initialize those tracking objects to play
            # nice with tensorflow keras
            s.run(tf.global_variables_initializer())
            log.debug(model.summary())
            history = model.fit(train_x, train_y,
                    epochs=self.epochs,
                    batch_size=self.batch_size,
                    validation_data = (val_x, val_y),
                    verbose=0,
                    callbacks=[TensorBoard(log_dir='/logs/{}'.format(self.config.output().path.replace('.yaml',''))),
                                EarlyStopping(patience=5, min_delta=.002)]
                    )

            evaluation = model.evaluate(test_x, test_y, batch_size=self.batch_size)

            with self.output().temporary_path() as f:
                p = Path(f)
                if not p.parent.exists():
                    os.makedirs(p.parent.as_posix())
                
                model.save_weights(f)
