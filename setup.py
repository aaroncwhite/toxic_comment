#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""
from ast import literal_eval
import os

from setuptools import setup, find_packages
from ast import literal_eval
DEV = literal_eval(os.environ.get("DEV", "0"))


def read(*names, **kwargs):
    try:
        with io.open(
            join(dirname(__file__), *names),
            encoding=kwargs.get('encoding', 'utf8')
        ) as fh:
            return fh.read()
    except:
        return ''


readme = read('README.rst')
history = read('HISTORY.rst')

requirements = [
    'pandas',
    'numpy',
    'tensorflow-gpu',
    'keras-metrics',
    'scikit-learn',
    'pyyaml',
    'luigi',
    'dask[dataframe]',
    'pyarrow',
    'matplotlib',
    'hyperopt'

]

setup_requirements = ['setuptools_scm',]

test_requirements = ['pytest', ]

scm_version = {"root": ".", "relative_to": __file__} if not DEV else False

setup(
    author="Aaron White",
    author_email='aaroncwhite@gmail.com',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.6',
    ],
    description="Models exploring the Kaggle toxic comment challenge from 2018.  More info- https://www.kaggle.com/c/jigsaw-toxic-comment-classification-challenge",
    install_requires=requirements,
    license="MIT license",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='toxic_comment',
    name='toxic-comment',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    use_scm_version=scm_version,
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://gitlab.com/aaroncwhite/toxic_comment',
    version='0.1.0',
    zip_safe=False,
)
