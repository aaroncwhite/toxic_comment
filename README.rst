==============================
Kaggle Toxic Comment Challenge
==============================

This repository showcases a Luigi_ based data pipeline to clean, tokenize, split, and train an NLP model.  I've incorporated the idea of 'salted workflows', which 
uses hashes of the code used plus hashed inputs to guarantee a unique model run if it has not been completed yet.  This works by leveraging Luigi's 
existing Directed Acyclic Graph structure and the concept of a Luigi Target to output data in a unique location per task.  

The idea for salted workflows came from Scott Gorlin.  His original example can be found here_.

.. _here: https://github.com/gorlins/salted

This also leverages my util-kit_ repository for things like the SaltedOutputs_ and WeightedLoss_ functions found in the task_ and model_ definitions.

.. _Luigi: https://luigi.readthedocs.io/en/stable/
.. _util-kit: https://gitlab.com/aaroncwhite/personal-util-kit
.. _SaltedOutputs: https://gitlab.com/aaroncwhite/personal-util-kit/blob/master/src/util_kit/luigi/descriptors/salted.py
.. _WeightedLoss: https://gitlab.com/aaroncwhite/personal-util-kit/blob/master/src/util_kit/modeling/losses.py
.. _task: src/toxic_comment/tasks/model.py
.. _model: src/toxic_comment/model/charlstm.py

My goal is to add multiple model definitions and allow for the best model selection.  

Caveat: this repository has been fun to build out; however, it is reinventing some wheels that tools like SageMaker_ handle better.  That said,
I have a computer at home with a GPU and that is much cheaper for my own personal projects than paying for time on AWS. 

.. _SageMaker: https://aws.amazon.com/sagemaker/


Installation
------------

This project uses Pipenv and Docker to ensure a reproducible environment.  

Setup with docker:

.. code-block:: bash

    docker-compose build
    docker-compose run app python -m toxic_comment

Setup with pipenv:

.. code-block:: bash

    pipenv install --dev
    pipenv run python -m toxic_comment

Note: the Docker configuration builds on the TensorFlow GPU enabled containers.  These make your life much easier when trying to deal with the 
right version of CUDA and CuDNN + Nvidia driver.  


Usage
-----

The entire application is packaged as a module with an entrypoint that runs the character based lstm.  I'd like to add out more to the command line 
interface and allow for some knobs that are configurable.  

.. code-block:: python

    python -m toxic_comment 


Note: currently, I'm facing an issue with the model training failing and haven't been able to chase down the issue.  


Background
----------

Models exploring the Kaggle toxic comment challenge from 2018.  More info- https://www.kaggle.com/c/jigsaw-toxic-comment-classification-challenge


* Free software: MIT license


Features
--------

* Luigi data pipeline
* Tensorflow Keras models
* Salted workflows


TODO
----

* Add more tests.  Currently only testing model creation.
* Add prediction task once best model is selected
* Finalize TPE hyperparameter search task
* Finalize grid search task 


Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://gitlab.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://gitlab.com/audreyr/cookiecutter-pypackage
